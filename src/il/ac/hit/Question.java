/*
 *	Author Ohad Cohen 
 */
package il.ac.hit;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

/**
 * The Class Question.
 */
public class Question
{
	
	/** The question. */
	private String question;
	
	/** The grammars. */
	private List<Grammar> grammars = new ArrayList<>();

	/**
	 * Instantiates a new question.
	 */
	public Question()
	{
		super();
	}

	/**
	 * Instantiates a new question.
	 *
	 * @param question the question
	 */
	public Question(String question)
	{
		super();
		setQuestion(question);
	}

	/**
	 * should check which lexicon has best match
	 * returns null in case question didn't initialize.
	 *
	 * @return the grammar
	 */
	public Grammar decomposeQuestion()
	{
		if (getQuestion() != null)
		{
			for(Grammar grammar : grammars)
			{
				grammar.getPercentageMatch(getQuestion());
			}
			return getMaxGrammar();
		}
		return null;
	}

	/**
	 * Gets the grammars from file.
	 *
	 * @return the grammars from file
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public void getGrammarsFromFile() throws IOException
	{
		String path = getClass().getClassLoader().getResource("QuestionsJSON.txt").toString();
		path = path.replace("%20", " ");
		URL url = new URL(path);
		InputStream file = url.openStream();
		Map<String, String> map = new Gson().fromJson(new InputStreamReader(file, "UTF-8"),
				new TypeToken<Map<String, String>>()
				{
				}.getType());

		for (eSearchFor searchOpt : eSearchFor.values())
		{
			Grammar G = null;
			String capitalCaseCategory = searchOpt.toString().toLowerCase();
			capitalCaseCategory = capitalCaseCategory.replace(capitalCaseCategory.charAt(0),
					Character.toUpperCase(capitalCaseCategory.charAt(0)));
			String[] questions = map.get(capitalCaseCategory).split(",");

			for (String str : questions)
			{
				str = str.replace("\n", "");
				str = str.replace("\r", "");
				G = new Grammar(str, searchOpt);
				grammars.add(G);
			}
		}

		file.close();
	}

	/**
	 * Gets the max grammar.
	 *
	 * @return the max grammar
	 */
	public Grammar getMaxGrammar()
	{
		Grammar gr = null;
		for (Grammar grammar : grammars)
		{
			if (gr == null
					|| gr.getMatchPercentage().getPercentageMatch() < grammar.getMatchPercentage().getPercentageMatch())
			{
				gr = grammar;
			}
		}
		return gr;
	}

	/**
	 * Gets the question.
	 *
	 * @return the question
	 */
	public String getQuestion()
	{
		return question;
	}

	/**
	 * Sets the question.
	 *
	 * @param question the new question
	 */
	public void setQuestion(String question)
	{
		this.question = question;
	}
}