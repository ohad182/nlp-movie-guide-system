/*
 *	Author Ohad Cohen 
 */
package il.ac.hit;

/**
 * The Enum eLexiconType.
 * 
 * Type choices: Optional/Single/Multiple
 */
public enum eLexiconType
{

	OPTIONAL, SINGLE, MULTIPLE
}
