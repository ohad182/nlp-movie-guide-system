/*
 *	Author Ohad Cohen 
 */
package il.ac.hit.moviedata;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

/**
 * The Class JsonParser.
 */
public class JsonParser
{
	
	/** The movie. */
	private Movie movie;
	
	/** The movie name. */
	private String movieName;
	
	/** The movie year. */
	private String movieYear;
	
	/** The url. */
	private String url = "http://www.omdbapi.com/?t=";

	/**
	 * Instantiates a new json parser.
	 *
	 * @param movieName the movie name
	 */
	public JsonParser(String movieName)
	{
		super();
		this.movieName = movieName;
	}

	/**
	 * Gets the movie name.
	 *
	 * @return the movie name
	 */
	public String getMovieName()
	{
		return movieName;
	}

	/**
	 * Sets the movie name.
	 *
	 * @param movieName the new movie name
	 */
	public void setMovieName(String movieName)
	{
		this.movieName = movieName;
	}

	/**
	 * Gets the movie year.
	 *
	 * @return the movie year
	 */
	public String getMovieYear()
	{
		return movieYear;
	}

	/**
	 * Sets the movie year.
	 *
	 * @param movieYear the new movie year
	 */
	public void setMovieYear(String movieYear)
	{
		this.movieYear = movieYear;
	}

	/**
	 * Instantiates a new json parser.
	 *
	 * @param movieName the movie name
	 * @param movieYear the movie year
	 */
	public JsonParser(String movieName, String movieYear)
	{
		super();
		this.movieName = movieName;
		this.movieYear = movieYear;
	}
	
	/**
	 * Gets the movie.
	 *
	 * @return the movie
	 */
	public Movie getMovie()
	{
		return movie;
	}

	/**
	 * Gets the movie from api.
	 *
	 * @return the movie from api
	 */
	public Movie getMovieFromAPI()
	{
		try
		{
			InputStream input = null;
			movieName = URLEncoder.encode(movieName, "UTF-8");
			if (movieYear != null)
			{
				movieYear = URLEncoder.encode(movieYear, "UTF-8");
			}
			String web = url + movieName + "&y=" + movieYear;
			input = new URL(web).openStream();
			Map<String, String> map = new Gson().fromJson(new InputStreamReader(input, "UTF-8"),
					new TypeToken<Map<String, String>>()
					{
					}.getType());
			movie = new Movie();

			movie.setError(map.get("Error"));
			movie.setResponse(map.get("Response"));
			movie.setTitle(map.get("Title"));
			movie.setYear(map.get("Year"));
			movie.setReleased(map.get("Released"));
			movie.setRuntime(map.get("Runtime"));
			movie.setGenre(map.get("Genre"));
			movie.setActors(map.get("Actors"));
			movie.setPlot(map.get("Plot"));
			movie.setImdbRating(map.get("imdbRating"));
			movie.setPoster(map.get("Poster"));
			movie.setDirector(map.get("Director"));
			movie.setRated(map.get("Rated"));
			movie.setAwards(map.get("Awards"));
			movie.setType(map.get("Type"));
		}
		catch (JsonIOException | JsonSyntaxException | IOException e)
		{
			System.out.println(e);
		}
		if (movie != null)
		{
			return movie;
		}
		else
		{
			return null;
		}
	}
}