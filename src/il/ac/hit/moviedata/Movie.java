/*
 *	Author Ohad Cohen 
 */
package il.ac.hit.moviedata;

import il.ac.hit.eSearchFor;

/**
 * The Class Movie.
 */
public class Movie
{
	
	/** The title. */
	private String title;
	
	/** The year. */
	private String year;
	
	/** The released. */
	private String released;
	
	/** The runtime. */
	private String runtime;
	
	/** The genre. */
	private String genre;
	
	/** The actors. */
	private String actors;
	
	/** The plot. */
	private String plot;
	
	/** The imdb rating. */
	private String imdbRating;
	
	/** The poster. */
	private String poster;
	
	/** The director. */
	private String director;
	
	/** The rated. */
	private String rated;
	
	/** The awards. */
	private String awards;
	
	/** The response. */
	private String response;
	
	/** The error. */
	private String error;
	
	/** The type. */
	private String type;
	
	/** The context. */
	private String context;

	/**
	 * Instantiates a new movie.
	 */
	public Movie()
	{
		super();
	}

	/**
	 * Override toString from object.
	 *
	 * @return Text representation of the Movie Class.
	 */
	@Override
	public String toString()
	{
		return "Movie [title=" + title + ", year=" + year + ", released=" + released + ", runtime=" + runtime
				+ ", genre=" + genre + ", actors=" + actors + ", plot=" + plot + ", imdbRating=" + imdbRating
				+ ", poster=" + poster + ", director=" + director + ", rated=" + rated + ", awards=" + awards
				+ ", response=" + response + ", error=" + error + ", type=" + type + "]";
	}

	/**
	 * Gets the context.
	 *
	 * @return the context
	 */
	public String getContext()
	{
		return context;
	}

	/**
	 * Sets the context.
	 *
	 * @param context the new context
	 */
	public void setContext(String context)
	{
		this.context = context;
	}
	
	/**
	 * Gets the type.
	 *
	 * @return the type
	 */
	public String getType()
	{
		return type;
	}

	/**
	 * Sets the type.
	 *
	 * @param type the new type
	 */
	public void setType(String type)
	{
		this.type = type;
	}

	/**
	 * Gets the response.
	 *
	 * @return the response
	 */
	public String getResponse()
	{
		return response;
	}

	/**
	 * Sets the response.
	 *
	 * @param response the new response
	 */
	public void setResponse(String response)
	{
		this.response = response;
	}

	/**
	 * Gets the error.
	 *
	 * @return the error
	 */
	public String getError()
	{
		return this.error;
	}

	/**
	 * Sets the error.
	 *
	 * @param error the new error
	 */
	public void setError(String error)
	{
		this.error = error;
	}

	/**
	 * Gets the rated.
	 *
	 * @return the rated
	 */
	public String getRated()
	{
		return rated;
	}

	/**
	 * Sets the rated.
	 *
	 * @param rated the new rated
	 */
	public void setRated(String rated)
	{
		this.rated = rated;
	}

	/**
	 * Gets the awards.
	 *
	 * @return the awards
	 */
	public String getAwards()
	{
		return awards;
	}

	/**
	 * Sets the awards.
	 *
	 * @param awards the new awards
	 */
	public void setAwards(String awards)
	{
		this.awards = awards;
	}

	/**
	 * Gets the director.
	 *
	 * @return the director
	 */
	public String getDirector()
	{
		return director;
	}

	/**
	 * Sets the director.
	 *
	 * @param director the new director
	 */
	public void setDirector(String director)
	{
		this.director = director;
	}

	/**
	 * Gets the title.
	 *
	 * @return the title
	 */
	public String getTitle()
	{
		return title;
	}

	/**
	 * Sets the title.
	 *
	 * @param title the new title
	 */
	public void setTitle(String title)
	{
		this.title = title;
	}

	/**
	 * Gets the year.
	 *
	 * @return the year
	 */
	public String getYear()
	{
		return year;
	}

	/**
	 * Sets the year.
	 *
	 * @param year the new year
	 */
	public void setYear(String year)
	{
		this.year = year;
	}

	/**
	 * Gets the released.
	 *
	 * @return the released
	 */
	public String getReleased()
	{
		return released;
	}

	/**
	 * Sets the released.
	 *
	 * @param released the new released
	 */
	public void setReleased(String released)
	{
		this.released = released;
	}

	/**
	 * Gets the runtime.
	 *
	 * @return the runtime
	 */
	public String getRuntime()
	{
		return runtime;
	}

	/**
	 * Sets the runtime.
	 *
	 * @param runtime the new runtime
	 */
	public void setRuntime(String runtime)
	{
		this.runtime = runtime;
	}

	/**
	 * Gets the genre.
	 *
	 * @return the genre
	 */
	public String getGenre()
	{
		return genre;
	}

	/**
	 * Sets the genre.
	 *
	 * @param genre the new genre
	 */
	public void setGenre(String genre)
	{
		this.genre = genre;
	}

	/**
	 * Gets the actors.
	 *
	 * @return the actors
	 */
	public String getActors()
	{
		return actors;
	}

	/**
	 * Sets the actors.
	 *
	 * @param actors the new actors
	 */
	public void setActors(String actors)
	{
		this.actors = actors;
	}

	/**
	 * Gets the plot.
	 *
	 * @return the plot
	 */
	public String getPlot()
	{
		return plot;
	}

	/**
	 * Sets the plot.
	 *
	 * @param plot the new plot
	 */
	public void setPlot(String plot)
	{
		this.plot = plot;
	}

	/**
	 * Gets the imdb rating.
	 *
	 * @return the imdb rating
	 */
	public String getImdbRating()
	{
		return imdbRating;
	}

	/**
	 * Sets the imdb rating.
	 *
	 * @param imdbRating the new imdb rating
	 */
	public void setImdbRating(String imdbRating)
	{
		this.imdbRating = imdbRating;
	}

	/**
	 * Gets the poster.
	 *
	 * @return the poster
	 */
	public String getPoster()
	{
		return poster;
	}

	/**
	 * Sets the poster.
	 *
	 * @param poster the new poster
	 */
	public void setPoster(String poster)
	{
		this.poster = poster;
	}

	/**
	 * Gets the answer.
	 *
	 * @param category the category
	 * @return the answer
	 */
	public String getAnswer(eSearchFor category)
	{
		StringBuilder builder = new StringBuilder();
		if (category == eSearchFor.ACTOR)
		{
			String[] actors = getActors().split(",");
			if (actors != null)
			{
				setContext("The main actor in " + getTitle() + " is:");
				return  actors[0];
			}
			else
				return "not found";
		}
		else if (category == eSearchFor.ACTORS)
		{
			if (this.getActors() != null)
			{
				String[] actors = getActors().split(",");
				setContext("The actors of " + getTitle() + " are:");
				builder.append("<html>");
				for (String str : actors)
					builder.append(str + "<br>");
				builder.append("</html");
				return builder.toString();
			}
			else
			{
				return null;
			}
		}
		else if (category == eSearchFor.DIRECTOR)
		{
			if (this.getDirector() != null)
			{
				String[] directors = getDirector().split(",");
				if (directors.length == 1)
				{
					setContext("The director of " + getTitle() + " is:");
					return directors[0];
				}
				else
				{
					setContext("The directors of " + getTitle() + " are:");
					builder.append("<html>");
					for (String str : directors)
						builder.append("		" + str + "<br>");
					builder.append("</html>");
				}
				return builder.toString();
			}
			else
			{
				return null;
			}
		}
		else if (category == eSearchFor.RUNTIME)
		{
			setContext("The runtime of " + getTitle() + " is:");
			return getRuntime();
		}
		else if (category == eSearchFor.RATING)
		{
			setContext("The rating of " + getTitle() + " is:");
			return getImdbRating();
		}
		else if (category == eSearchFor.RELEASED)
		{
			setContext("The movie " + getTitle() + " released at:");
			builder.append(getReleased());
			return builder.toString().trim();
		}
		return null;
	}
}