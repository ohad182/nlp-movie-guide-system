/*
 *	Author Ohad Cohen 
 */
package il.ac.hit.omdbgui;

import java.awt.*;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import il.ac.hit.Grammar;
import il.ac.hit.Question;
import il.ac.hit.moviedata.*;
import java.awt.event.*;
import java.io.IOException;

/**
 * The Class OmdbGUI.
 */
public class OmdbGUI
{
	private JFrame frmQuestions;
	private JTextField tfQuestion;
	private JsonParser movieParser = null;
	private Movie movie = null;
	private JLabel lblAnswer;
	private Grammar gr;
	private JLabel lblIntro;

	/**
	 * Launch the application.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args)
	{
		EventQueue.invokeLater(new Runnable()
		{
			public void run()
			{
				try
				{
					OmdbGUI window = new OmdbGUI();
					Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
					window.frmQuestions.setLocation(dim.width / 2 - window.frmQuestions.getSize().width / 2,
							dim.height / 2 - window.frmQuestions.getSize().height / 2);
					window.frmQuestions.setVisible(true);
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 *
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public OmdbGUI() throws IOException
	{
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 *
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	private void initialize() throws IOException
	{

		frmQuestions = new JFrame();
		frmQuestions.setTitle("Smart Movie Guide");
		frmQuestions.setResizable(false);
		frmQuestions.setBounds(100, 100, 450, 230);
		frmQuestions.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmQuestions.getContentPane().setLayout(null);
		Image img = ImageIO.read(this.getClass().getClassLoader().getResource("Button-Play-icon.png"));
		frmQuestions.setIconImage(img);

		tfQuestion = new JTextField();
		tfQuestion.getDocument().addDocumentListener(new DocumentListener()
		{

			@Override
			public void removeUpdate(DocumentEvent e)
			{
				if (tfQuestion.getText().length() == 0)
				{
					lblAnswer.setText("<html>Hi!<br/>I am your movie guide</html>");
					lblIntro.setText("");
					if (MovieInfo.getInstance().getVisible() == true)
					{
						MovieInfo.getInstance().hide();
					}
				}

			}

			@Override
			public void insertUpdate(DocumentEvent e)
			{
			}

			@Override
			public void changedUpdate(DocumentEvent e)
			{
			}
		});
		tfQuestion.setBounds(10, 37, 313, 20);
		frmQuestions.getContentPane().add(tfQuestion);
		tfQuestion.setColumns(10);

		JButton btnAskQuestion = new JButton("Ask");
		SwingUtilities.getRootPane(tfQuestion).setDefaultButton(btnAskQuestion);
		btnAskQuestion.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent arg0)
			{
				if (tfQuestion.getText() != null && tfQuestion.getText().length() > 0)
				{
					Question question = new Question(tfQuestion.getText());
					try
					{
						question.getGrammarsFromFile();
						gr = question.decomposeQuestion();
						movieParser = new JsonParser(gr.getMovieTitle());
					}
					catch (IOException e)
					{
						e.printStackTrace();
					}

					movie = movieParser.getMovieFromAPI();
					if (movie == null)
					{
						String name = gr.getMovieTitle();
						if (name.startsWith("the") && name.lastIndexOf("the") > 0)
						{
							name = "the " + name.replace("the", "").trim();
						}
						movieParser.setMovieName(name);
						movie = movieParser.getMovieFromAPI();
					}
					Thread t = new Thread()
					{
						public void run()
						{
							String answer;
							if ((answer = movie.getAnswer(gr.getCategory())) != null)
							{
								lblAnswer.setText(answer);
								lblAnswer.setToolTipText(answer);
								lblIntro.setText(movie.getContext());
								MovieInfo.getInstance().show(movie);
							}
							else
							{
								JOptionPane.showMessageDialog(null, "Cant find what you are looking for, try again.");
							}
						}
					};
					t.setDaemon(true);
					t.start();
				}
				else
				{
					JOptionPane.showMessageDialog(null, "You haven't asked me anything...");
				}
			}
		});
		btnAskQuestion.setBounds(333, 36, 89, 23);
		frmQuestions.getContentPane().add(btnAskQuestion);

		JLabel lblQuestion = new JLabel("Enter your question:");
		lblQuestion.setBounds(10, 20, 162, 14);
		frmQuestions.getContentPane().add(lblQuestion);

		lblAnswer = new JLabel("<html>Hi!<br/>I am your movie guide</html>");
		lblAnswer.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblAnswer.setVerticalAlignment(SwingConstants.TOP);
		lblAnswer.setHorizontalAlignment(SwingConstants.CENTER);
		lblAnswer.setBounds(10, 88, 424, 86);
		frmQuestions.getContentPane().add(lblAnswer);

		lblIntro = new JLabel("");
		lblIntro.setBounds(10, 68, 424, 20);
		frmQuestions.getContentPane().add(lblIntro);

		JMenuBar menuBar = new JMenuBar();
		frmQuestions.setJMenuBar(menuBar);

		JMenu mnExamples = new JMenu("Examples");
		menuBar.add(mnExamples);

		JMenuItem mntmActor = new JMenuItem("Actor");
		mntmActor.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				String actorExamples = "You can ask me the following templates:\n\n"
						+ "in the movie *TITLE* who was/took/played/act/acted the main/leading role/act/actor\n\n"
						+ "in the *TITLE* who was/took/played/act/acted the main/leading role/act/actor\n\n"
						+ "in *TITLE* who was/took/played/act/acted the main/leading role/act/actor\n\n"
						+ "who took/played/acted/act the main/leading role/actor in the movie *TITLE*\n\n"
						+ "who took/played/acted/act the main/leading role/actor in the *TITLE*\n\n"
						+ "who took/played/acted/act the main/leading role/actor in *TITLE*\n\n"
						+ "who is/was the main/leading/lead actor in the film/movie *TITLE*\n\n"
						+ "who is/was the main/leading/lead actor in film/movie *TITLE*\n\n";
				JOptionPane.showMessageDialog(null, actorExamples, "Actor Examples", JOptionPane.INFORMATION_MESSAGE,
						null);
			}
		});
		mnExamples.add(mntmActor);

		JMenuItem mntmActors = new JMenuItem("Actors");
		mntmActors.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				String actorsExamples = "You can ask me the following templates:\n\n"
						+ "name the cast/actors of/in *TITLE*\n\n" + "name all the cast/actors of/in *TITLE*\n\n"
						+ "who are all the actors in *TITLE*\n\n" + "who is/were the cast of *TITLE*\n\n"
						+ "in the *TITLE* who are the actors/cast\n\n"
						+ "in the movie *TITLE* who are the actors/cast\n\n" + "in *TITLE* who are the actors/cast\n\n";
				JOptionPane.showMessageDialog(null, actorsExamples, "Actors Examples", JOptionPane.INFORMATION_MESSAGE,
						null);
			}
		});
		mnExamples.add(mntmActors);

		JMenuItem mntmDirector = new JMenuItem("Director");
		mntmDirector.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				String directorExamples = "You can ask me the following templates:\n\n"
						+ "who's/whos the director of *TITLE*\n\n" + "who is the director of *TITLE*\n\n"
						+ "who directed *TITLE*\n\n";
				JOptionPane.showMessageDialog(null, directorExamples, "Director Examples",
						JOptionPane.INFORMATION_MESSAGE, null);
			}
		});
		mnExamples.add(mntmDirector);

		JMenuItem mntmReleased = new JMenuItem("Released");
		mntmReleased.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				String releasedExamples = "You can ask me the following templates:\n\n"
						+ "when was (the) *TITLE* released\n\n" + "what is the release date of *TITLE*\n\n";
				JOptionPane.showMessageDialog(null, releasedExamples, "Released Examples",
						JOptionPane.INFORMATION_MESSAGE, null);
			}
		});
		mnExamples.add(mntmReleased);

		JMenuItem mntmRuntime = new JMenuItem("Runtime");
		mntmRuntime.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				String runtimeExamples = "You can ask me the following templates:\n\n"
						+ "what is the duration of the *TITLE*\n\n" + "how long is *TITLE* is\n\n"
						+ "how long *TITLE* is\n\n" + "how long is *TITLE*\n\n";
				JOptionPane.showMessageDialog(null, runtimeExamples, "Runtime Examples",
						JOptionPane.INFORMATION_MESSAGE, null);
			}
		});
		mnExamples.add(mntmRuntime);

		JMenuItem mntmRating = new JMenuItem("Rating");
		mntmRating.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				String ratingExamples = "You can ask me the following templates:\n\n"
						+ "what is the rating of *TITLE*\n\n" + "what is the rating of the movie/film *TITLE*\n\n"
						+ "show me the rating of *TITLE*\n\n" + "show me the rating of the movie/film *TITLE*\n\n";
				JOptionPane.showMessageDialog(null, ratingExamples, "Rating Examples", JOptionPane.INFORMATION_MESSAGE,
						null);
			}
		});
		mnExamples.add(mntmRating);

		JMenuItem mntmAbout = new JMenuItem("About");
		mntmAbout.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				String about = "Developed & Designed by Ohad Cohen and Micael Gorlik";
				JOptionPane.showMessageDialog(null, about, "About", JOptionPane.INFORMATION_MESSAGE, null);
			}
		});
		menuBar.add(mntmAbout);
	}
}