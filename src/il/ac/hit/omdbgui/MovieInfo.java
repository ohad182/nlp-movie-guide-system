/*
 *	Author Ohad Cohen 
 */
package il.ac.hit.omdbgui;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;

import javax.imageio.ImageIO;
import javax.swing.*;

import il.ac.hit.moviedata.Movie;
import java.awt.Font;
import javax.swing.SwingConstants;

/**
 * The Class MovieInfo.
 */
public class MovieInfo
{
	private JFrame frmMovieInfo;
	JLabel lblImage = null;
	JLabel labelMovieName = null;
	private JLabel labelMovieTitle;
	private JLabel lblYear;
	private JLabel labelPutYear;
	private JLabel lblReleased;
	private JLabel lblRuntime;
	private JLabel labelPutRuntime;
	private JLabel lblGenre;
	private JLabel labelPutGenre;
	private JLabel lblRating;
	private JLabel labelPutRating;
	private JLabel lblActors;
	private JLabel labelPutActor1;
	private JLabel labelPutActor2;
	private JLabel labelPutActor3;
	private JLabel labelPutActor4;
	private JLabel labelPutActor5;
	private JLabel lblAwards;
	private JLabel lblPutAwards;
	private JLabel lblRated;
	private JLabel lblPutRated;
	private JLabel lblDirectors;
	private JLabel lblPutDirector1;
	private JLabel lblPutDirector2;
	private JLabel lblPutDirector3;
	
	/** The instance. */
	private static volatile MovieInfo instance;
	
	/** The movie. */
	private Movie movie;

	/**
	 * Gets the single instance of MovieInfo.
	 *
	 * @return single instance of MovieInfo
	 */
	public synchronized static MovieInfo getInstance()
	{
		if (instance == null)
		{
			instance = new MovieInfo();
		}
		return instance;
	}

	/**
	 * Launch the application.
	 *
	 * @param movie the movie
	 * @wbp.parser.entryPoint 
	 */
	public void show(Movie movie)
	{
		EventQueue.invokeLater(new Runnable()
		{
			public void run()
			{
				try
				{
					setMovie(movie);
					initialize();
					frmMovieInfo.setVisible(true);
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
		});
	}
	
	/**
	 * Hide.
	 */
	public void hide()
	{
		frmMovieInfo.setVisible(false);
	}
	
	/**
	 * Gets the visible.
	 *
	 * @return the visible state
	 */
	public boolean getVisible()
	{
		return frmMovieInfo.isVisible();
	}

	/**
	 * Create the application.
	 */
	private MovieInfo()
	{
		frmMovieInfo = new JFrame("Movie Info");
	}

	/**
	 * Initialize the contents of the frame.
	 *
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	private void initialize() throws IOException
	{
		String response = "False";
		if (frmMovieInfo != null)
		{
			frmMovieInfo.dispose();
		}

		frmMovieInfo = new JFrame("Movie Info");
		Image icon = ImageIO.read(this.getClass().getClassLoader().getResource("movie-icon.png"));
		frmMovieInfo.setIconImage(icon);
		frmMovieInfo.setResizable(false);
		frmMovieInfo.setBounds(100, 100, 282, 596);
		frmMovieInfo.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frmMovieInfo.getContentPane().setLayout(null);

		if (!movie.getResponse().toLowerCase().equals(response.toLowerCase()))
		{
			String noPoster = "N/A";
			URL imageURL = (movie.getPoster() != null && !movie.getPoster().equals(noPoster)) ? new URL(movie.getPoster()): this.getClass().getClassLoader().getResource("noimage.jpg");

			lblImage = new JLabel("");
			lblImage.setHorizontalAlignment(SwingConstants.CENTER);
			lblImage.setBounds(52, 45, 160, 160);
			BufferedImage bi = new BufferedImage(lblImage.getWidth(), lblImage.getHeight(), BufferedImage.TYPE_INT_RGB);
			Graphics2D g = bi.createGraphics();
			Image img = null;
				img = ImageIO.read(imageURL);
			g.drawImage(img, 0, 0, lblImage.getWidth(), lblImage.getHeight(), null);
			g.dispose();
			lblImage.setIcon(new ImageIcon(bi));
			frmMovieInfo.getContentPane().add(lblImage);

			labelMovieTitle = new JLabel();
			labelMovieTitle.setText(movie.getTitle());
			labelMovieTitle.setToolTipText(movie.getTitle());
			labelMovieTitle.setHorizontalAlignment(SwingConstants.CENTER);
			labelMovieTitle.setFont(new Font("Tahoma", Font.BOLD, 14));
			labelMovieTitle.setBounds(10, 11, 256, 23);
			frmMovieInfo.getContentPane().add(labelMovieTitle);

			lblYear = new JLabel("Released year: ");
			lblYear.setBounds(10, 213, 111, 14);
			frmMovieInfo.getContentPane().add(lblYear);

			labelPutYear = new JLabel(movie.getYear());
			labelPutYear.setBounds(131, 213, 135, 14);
			frmMovieInfo.getContentPane().add(labelPutYear);

			lblReleased = new JLabel("Released at: ");
			lblReleased.setBounds(10, 238, 82, 14);
			frmMovieInfo.getContentPane().add(lblReleased);

			JLabel lblPutRelease = new JLabel(movie.getReleased());
			lblPutRelease.setBounds(99, 238, 167, 14);
			frmMovieInfo.getContentPane().add(lblPutRelease);

			lblRuntime = new JLabel("Runtime:");
			lblRuntime.setBounds(10, 263, 73, 14);
			frmMovieInfo.getContentPane().add(lblRuntime);

			labelPutRuntime = new JLabel(movie.getRuntime());
			labelPutRuntime.setBounds(99, 263, 167, 14);
			frmMovieInfo.getContentPane().add(labelPutRuntime);

			lblGenre = new JLabel("Genre: ");
			lblGenre.setBounds(10, 288, 46, 14);
			frmMovieInfo.getContentPane().add(lblGenre);

			labelPutGenre = new JLabel(movie.getGenre());
			labelPutGenre.setBounds(78, 288, 188, 14);
			frmMovieInfo.getContentPane().add(labelPutGenre);

			lblRating = new JLabel("Rating: ");
			lblRating.setBounds(10, 313, 46, 14);
			frmMovieInfo.getContentPane().add(lblRating);

			labelPutRating = new JLabel(movie.getImdbRating());
			labelPutRating.setBounds(78, 313, 188, 14);
			frmMovieInfo.getContentPane().add(labelPutRating);

			lblActors = new JLabel("Actors:");
			lblActors.setBounds(10, 412, 46, 14);
			frmMovieInfo.getContentPane().add(lblActors);

			lblAwards = new JLabel("Awards:");
			lblAwards.setBounds(10, 513, 58, 14);
			frmMovieInfo.getContentPane().add(lblAwards);

			lblPutAwards = new JLabel(movie.getAwards());
			lblPutAwards.setBounds(75, 513, 191, 14);
			frmMovieInfo.getContentPane().add(lblPutAwards);

			lblRated = new JLabel("Rated: ");
			lblRated.setBounds(10, 538, 46, 14);
			frmMovieInfo.getContentPane().add(lblRated);

			lblPutRated = new JLabel(movie.getRated());
			lblPutRated.setBounds(78, 538, 188, 14);
			frmMovieInfo.getContentPane().add(lblPutRated);

			lblDirectors = new JLabel("Director(s): ");
			lblDirectors.setBounds(10, 338, 73, 14);
			frmMovieInfo.getContentPane().add(lblDirectors);

			String[] directors = movie.getDirector().split(",");

			if (directors.length > 0)
			{
				lblPutDirector1 = new JLabel(directors[0]);
				lblPutDirector1.setBounds(99, 338, 167, 14);
				frmMovieInfo.getContentPane().add(lblPutDirector1);
			}
			if (directors.length > 1)
			{
				lblPutDirector2 = new JLabel(directors[1]);
				lblPutDirector2.setBounds(99, 363, 167, 14);
				frmMovieInfo.getContentPane().add(lblPutDirector2);
			}
			if (directors.length > 2)
			{
				lblPutDirector3 = new JLabel(directors[2]);
				lblPutDirector3.setBounds(99, 388, 167, 14);
				frmMovieInfo.getContentPane().add(lblPutDirector3);
			}
			setActors(movie.getActors());
		}
		else
		{
			URL imageURL = this.getClass().getClassLoader().getResource("Error.png");

			lblImage = new JLabel();
			lblImage.setHorizontalAlignment(SwingConstants.CENTER);
			lblImage.setBounds(52, 45, 160, 160);
			BufferedImage bi = new BufferedImage(lblImage.getWidth(), lblImage.getHeight(), BufferedImage.TYPE_INT_RGB);
			Graphics2D g = bi.createGraphics();
			Image img = ImageIO.read(imageURL);
			g.drawImage(img, 0, 0, lblImage.getWidth(), lblImage.getHeight(), null);
			g.dispose();
			lblImage.setIcon(new ImageIcon(bi));
			frmMovieInfo.getContentPane().add(lblImage);

			labelMovieTitle = new JLabel();
			labelMovieTitle.setText(movie.getError());
			labelMovieTitle.setToolTipText(movie.getError());
			labelMovieTitle.setHorizontalAlignment(SwingConstants.CENTER);
			labelMovieTitle.setFont(new Font("Tahoma", Font.BOLD, 14));
			labelMovieTitle.setBounds(10, 11, 256, 23);
			frmMovieInfo.getContentPane().add(labelMovieTitle);

		}
	}

	/**
	 * Sets the actors.
	 *
	 * @param actors the new actors
	 */
	public void setActors(String actors)
	{
		String[] splitted = actors.split(",");

		if (splitted.length > 0)
		{
			labelPutActor1 = new JLabel(splitted[0].trim());
			labelPutActor1.setBounds(78, 412, 188, 14);
			frmMovieInfo.getContentPane().add(labelPutActor1);
		}
		if (splitted.length > 1)
		{
			labelPutActor2 = new JLabel(splitted[1].trim());
			labelPutActor2.setBounds(78, 437, 188, 14);
			frmMovieInfo.getContentPane().add(labelPutActor2);
		}
		if (splitted.length > 2)
		{
			labelPutActor3 = new JLabel(splitted[2].trim());
			labelPutActor3.setBounds(78, 462, 188, 14);
			frmMovieInfo.getContentPane().add(labelPutActor3);
		}
		if (splitted.length > 3)
		{
			labelPutActor4 = new JLabel("");
			labelPutActor4.setBounds(78, 488, 188, 14);
			frmMovieInfo.getContentPane().add(labelPutActor4);
		}
		if (splitted.length > 3)
		{
			labelPutActor5 = new JLabel("");
			labelPutActor5.setBounds(78, 513, 188, 14);
			frmMovieInfo.getContentPane().add(labelPutActor5);
		}
	}

	/**
	 * Gets the movie.
	 *
	 * @return the movie
	 */
	public Movie getMovie()
	{
		return this.movie;
	}

	/**
	 * Sets the movie.
	 *
	 * @param movie the new movie
	 */
	public void setMovie(Movie movie)
	{
		this.movie = movie;
	}
}