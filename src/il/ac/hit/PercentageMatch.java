/*
 *	Author Ohad Cohen 
 */
package il.ac.hit;

/**
 * The Class PercentageMatch.
 */
public class PercentageMatch
{
	
	/** The total. */
	private double total;
	
	/** The matching. */
	private double matching;

	/**
	 * Instantiates a new percentage match.
	 *
	 * @param total the total
	 */
	public PercentageMatch(int total)
	{
		setTotal(total);
	}

	/**
	 * Gets the percentage match.
	 *
	 * @return the percentage match
	 */
	public double getPercentageMatch()
	{
		if (getTotal() > 0)
		{
			return getMatching() / getTotal();
		}
		return 0;
	}

	/**
	 * Adds the match.
	 */
	public void addMatch()
	{
		setMatching(getMatching() + 1);
	}

	/**
	 * Adds the match.
	 *
	 * @param points the points
	 */
	public void addMatch(int points)
	{
		setMatching(getMatching() + points);
	}

	/**
	 * Override toString from object.
	 *
	 * @return Text representation of the PercentageMatch Class.
	 */
	@Override
	public String toString()
	{
		return "PercentageMatch [matching=" + matching + ", total=" + total + "]";
	}

	/**
	 * Gets the total.
	 *
	 * @return the total
	 */
	public double getTotal()
	{
		return total;
	}

	/**
	 * Sets the total.
	 *
	 * @param total the new total
	 */
	public void setTotal(double total)
	{
		this.total = total;
	}

	/**
	 * Gets the matching.
	 *
	 * @return the matching
	 */
	public double getMatching()
	{
		return matching;
	}

	/**
	 * Sets the matching.
	 *
	 * @param matching the new matching
	 */
	public void setMatching(double matching)
	{
		this.matching = matching;
	}
}