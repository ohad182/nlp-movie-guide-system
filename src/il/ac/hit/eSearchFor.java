/*
 *	Author Ohad Cohen 
 */
package il.ac.hit;

/**
 * The Enum eSearchFor.
 * 
 * Type choices: Actor/Actors/Director/Released/Runtime/Rating
 */
public enum eSearchFor
{
	ACTOR, ACTORS, DIRECTOR, RELEASED, RUNTIME, RATING
}
