/*
 *	Author Ohad Cohen 
 */
package il.ac.hit;

import java.util.*;

/**
 * The Class Grammar.
 */
public class Grammar
{
	
	/** The category. */
	private eSearchFor category;
	
	/** The movie title. */
	private StringBuilder movieTitle = new StringBuilder();
	
	/** The lexicons. */
	private List<Lexicon> lexicons = null;
	
	/** The match percentage. */
	private PercentageMatch matchPercentage;

	/**
	 * Instantiates a new grammar.
	 */
	public Grammar()
	{
	}

	/**
	 * Instantiates a new grammar.
	 *
	 * @param sentence the sentence
	 */
	public Grammar(String sentence)
	{
		lexicons = new ArrayList<Lexicon>();
		sentence = removeNonAlphas(sentence);
		String[] words = sentence.split(" ");
		String opt;
		for (int i = 0; i < words.length; i++)
		{
			opt = null;
			Lexicon lex;
			if (words[i].contains("("))
			{
				opt = "";
				while (!words[i].contains(")"))
				{
					opt = opt.concat(words[i] + " ");
					i++;
				}
				opt = opt.concat(words[i] + " ");
				opt = opt.trim();

				lex = new Lexicon(opt);
			}
			else
			{
				lex = new Lexicon(words[i]);
			}
			lexicons.add(lex);
		}
	}

	/**
	 * Instantiates a new grammar.
	 *
	 * @param sentence the sentence
	 * @param searchCategory the search category enum
	 */
	public Grammar(String sentence, eSearchFor searchCategory)
	{
		this(sentence);
		setCategory(searchCategory);
	}

	/**
	 * Removes the non alphas.
	 *
	 * @param sentence the sentence
	 * @return the string
	 */
	private String removeNonAlphas(String sentence)
	{
		sentence.replace("?", "");
		sentence.replace(",", "");
		sentence.replace(".", "");
		sentence.replace("!", "");
		return sentence;
	}

	/**
	 * Override toString from object.
	 *
	 * @return Text representation of the Grammar Class.
	 */
	@Override
	public String toString()
	{
		StringBuilder builder = new StringBuilder();
		builder.append("\nGrammar [category=" + category + "]" + "\n");
		for (Lexicon lex : lexicons)
		{
			builder.append(lex);
		}
		return builder.toString();
	}

	/**
	 * Gets the lexicons.
	 *
	 * @return the lexicons
	 */
	public List<Lexicon> getLexicons()
	{
		return lexicons;
	}

	/**
	 * Sets the lexicons.
	 *
	 * @param lexicons the new lexicons
	 */
	public void setLexicons(List<Lexicon> lexicons)
	{
		this.lexicons = lexicons;
	}

	/**
	 * Gets the category.
	 *
	 * @return the category
	 */
	public eSearchFor getCategory()
	{
		return category;
	}

	/**
	 * Sets the category.
	 *
	 * @param category the new category
	 */
	public void setCategory(eSearchFor category)
	{
		this.category = category;
	}

	/**
	 * Gets the movie title.
	 *
	 * @return the movie title
	 */
	public String getMovieTitle()
	{
		return movieTitle.toString().trim();
	}

	/**
	 * Sets the movie title.
	 *
	 * @param movieTitle the new movie title
	 */
	public void setMovieTitle(String movieTitle)
	{
		System.out.println("appending: " + movieTitle);
		this.movieTitle.append(movieTitle + " ");
	}

	/**
	 * Gets the match percentage.
	 *
	 * @return the match percentage
	 */
	public PercentageMatch getMatchPercentage()
	{
		return matchPercentage;
	}

	/**
	 * Sets the match percentage.
	 *
	 * @param matchPercentage the new match percentage
	 */
	public void setMatchPercentage(PercentageMatch matchPercentage)
	{
		this.matchPercentage = matchPercentage;
	}

	/**
	 * Gets the percentage match.
	 *
	 * @param i_StringToMatch the string to match
	 * @return the percentage match
	 */
	public double getPercentageMatch(String stringToMatch)
	{
		String title = "*TITLE*";
		stringToMatch = removeNonAlphas(stringToMatch);
		String[] words = stringToMatch.split(" ");
		matchPercentage = new PercentageMatch(words.length);

		for (int i = 0, j = 0; i < words.length && j < lexicons.size();)
		{
			if (lexicons.get(j).isContains(words[i]))
			{
				if (lexicons.get(j).isContains(title))
				{
					setMovieTitle(words[i]);
				}
				if (i < words.length)
				{
					i++;
				}
				if (j < lexicons.size())
				{
					j++;
				}
				matchPercentage.addMatch();
			}
			else
			{
				if (lexicons.get(j).getLexType() == eLexiconType.OPTIONAL)
				{
					if (j < lexicons.size())
					{
						j++;
					}
				}
				else
				{
					if (lexicons.get(j).isContains(title))
					{
						setMovieTitle(words[i]);

						if (j + 1 < lexicons.size() && i + 1 < words.length)
						{
							if (lexicons.get(j + 1).isContains(words[i + 1]))
							{
								i++;
								j++;
							}
							else
							{
								i++;
								setMovieTitle(words[i]);
							}
						}
						else if (j == lexicons.size() - 1 && i < words.length)
						{
							i++;
						}
					}
					else
					{
						break;
					}
				}
			}
		}
		System.out.println(this);
		System.out.println("movie title before: " + getMovieTitle());
		System.out.println(matchPercentage);
		return matchPercentage.getPercentageMatch();
	}
}