/*
 *	Author Ohad Cohen 
 */
package il.ac.hit;

import java.util.Map;

/**
 * The Class Word.
 */
public class Word
{
	
	/** The word. */
	private String word;
	
	/** The siblings. */
	private Map<String, Integer> siblings;

	/**
	 * Instantiates a new word.
	 *
	 * @param str the word
	 */
	public Word(String str)
	{
		setWord(str);
	}

	/**
	 * Gets the word.
	 *
	 * @return the word
	 */
	public String getWord()
	{
		return word;
	}

	/**
	 * Sets the word.
	 *
	 * @param word the new word
	 */
	public void setWord(String word)
	{
		if (word != "" && word != null)
		{
			this.word = word;
		}
	}

	/**
	 * Gets the siblings.
	 *
	 * @return the siblings
	 */
	public Map<String, Integer> getSiblings()
	{
		return siblings;
	}

	/**
	 * Sets the siblings.
	 *
	 * @param siblings the siblings
	 */
	public void setSiblings(Map<String, Integer> siblings)
	{
		this.siblings = siblings;
	}

	/**
	 * Override toString from object.
	 *
	 * @return Text representation of the Word Class.
	 */
	@Override
	public String toString()
	{
		return word;
	}
}