/*
 *	Author Ohad Cohen 
 */
package il.ac.hit;

import java.util.ArrayList;
import java.util.List;

/**
 * The Class Lexicon.
 */
public class Lexicon
{
	
	/** The words. */
	private List<Word> words;
	
	/** The lexicon type. */
	private eLexiconType lexType;

	/**
	 * Instantiates a new lexicon.
	 *
	 * @param str the string to make lexicon from
	 */
	public Lexicon(String str)
	{
		String[] splitedWords = null;
		words = new ArrayList<Word>();
		if (str.contains("/"))
		{
			splitedWords = str.split("/");
			for (String word : splitedWords)
			{
				if (word != "" && word != null)
				{
					Word w = new Word(word);
					words.add(w);
				}
			}
			setLexType(eLexiconType.MULTIPLE);
		}
		else if (str.contains("(") && str.contains(")"))
		{
			str = str.replace("(", "");
			str = str.replace(")", "");
			splitedWords = str.split(" ");
			for (String word : splitedWords)
			{
				Word w = new Word(word);
				if (word != "" && word != null)
				{
					words.add(w);
				}
			}
			setLexType(eLexiconType.OPTIONAL);
		}
		else
		{
			Word w = new Word(str);
			words.add(w);
			setLexType(eLexiconType.SINGLE);
		}
	}

	/**
	 * Gets the lexicon type.
	 *
	 * @return the lexicon type
	 */
	public eLexiconType getLexType()
	{
		return lexType;
	}

	/**
	 * Sets the lexicon type.
	 *
	 * @param lexType the new lexicon type
	 */
	public void setLexType(eLexiconType lexType)
	{
		this.lexType = lexType;
	}

	/**
	 * Checks if the lexicon contains the word.
	 *
	 * @param word the word
	 * @return true, if is contains
	 */
	public boolean isContains(String word)
	{
		for (Word wrd : words)
		{
			if (wrd.getWord().contains(word))
			{
				return true;
			}
		}
		return false;
	}

	/**
	 * Gets the words.
	 *
	 * @return the words
	 */
	public List<Word> getWords()
	{
		return words;
	}

	/**
	 * Sets the words.
	 *
	 * @param words the new words
	 */
	public void setWords(List<Word> words)
	{
		this.words = words;
	}

	/**
	 * Override toString from object.
	 *
	 * @return Text representation of the Lexicon Class.
	 */
	@Override
	public String toString()
	{
		return "Lexicon [words=" + words + ", lexType=" + lexType + "]";
	}
}